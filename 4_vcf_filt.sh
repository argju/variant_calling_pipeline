#!/bin/bash
#SBATCH -N 1
#SBATCH -n 10
#SBATCH --partition=cigene,hugemem
#SBATCH -J vcf_filter
#SBATCH --output=slurm_logs/vcf_filter_%j.log

set -o nounset   # Prevent unset variables from been used.
set -o errexit   # Exit if error occurs

module load samtools ## Tim use his own bcftools in $PATH
vcf_in=$1
vcf_out=$(dirname $vcf_in)/$(basename ${vcf_in} .vcf.gz).filt.setID.vcf.gz
nt=$SLURM_CPUS_ON_NODE
#ref=$2

## Below filters are taken partly from the 1kbulls filters run 5.
## Reduced --SnpGap from 5 to 4, since freebayes is good at local realignment. 

###### NB! #######
## DP needs to be set manually. 
#################

## Erik Garrison recommandation: (QUAL / AO < 10)
# scaling quality by depth is like requiring that the additional log-unit contribution
# of each read is at least N

## Soft filter	--soft-filter 1kbulls "$vcf_in"

## Normalization and set-id fix weird freebayes Ref and Alt calls like AAC AAT (SNP), 
## and populates the ID field with '%CHROM\_%POS\_%REF\_%FIRST_ALT', which Beagle requires. 

echo
time bcftools filter \
	--include "(QUAL / AO > 10)  & SAF >= 1 & SAR >= 1 & RPR > 1 & RPL > 1 & DP > 100 & DP < 8000 & MAF > 0.05 & NUMALT == 1" \
	--SnpGap 20 \
	"$vcf_in" | \
	bcftools view \
	--types snps - | \
        bcftools annotate --set-id 'chr%CHROM\_%POS\_%REF\_%FIRST_ALT' -o $vcf_out -O z --threads 10 - && \
        tabix $vcf_out

	#bcftools +prune -l 0.8  -w 500kb -e "F_MISSING>=0.05" - | \
	#bcftools annotate --set-id 'chr%CHROM\_%POS\_%REF\_%FIRST_ALT' -o $vcf_out -O z --threads 10 - && \
	#tabix $vcf_out
# "QUAL / AO > 10 & SAF > 0 & SAR > 0 & RPR > 1 & RPL > 1"
#--exclude "(QUAL / AO < 10) | NUMALT >= 2 | SAF <= 1 | SAR <= 1 | DP < 10 | DP > 2200 | MAF < 0.01"         --SnpGap 4         --IndelGap 10 
