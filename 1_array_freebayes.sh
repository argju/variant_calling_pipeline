#!/bin/sh
#SBATCH -J freebayes-array
#SBATCH -n 1
#SBATCH --partition=smallmem,cigene,hugemem
#SBATCH -N 1
#SBATCH --output=slurm_logs/freebayes_array_%A_region%a.log

################## INSTRUCTIONS ##########################

## This script calls variants from multiple BAMs paralellised over 5 Mb chunks. 
## NB!! It requires that you make the DIR slurm_logs before you start. 

## Step 0 Set prefix. Folders is made by script.
prefix=variant_calling/ 
mkdir -p $prefix temp slurm  alignments \
	called_variants_FileLists merged_chrom_vcf chrN_stats

## STEP 1: Create a list with full path to every BAM, eg: find `pwd` -name "*.bam" > bamlist.txt 

## STEP 2: Create the 5 Mb regions to call in parallel over:
### OPTION 1 /local/genome/packages/freebayes/1.1.0/scripts/fasta_generate_regions.py reference_fasta.fa 5000000 > regionlist.txt
### OPTION 2 (coverage optimised):  goleft indexsplit --n 1000  *.bam 2>/dev/null | awk '{print $1":"$2"-"$3}' > ../regionlist.txt
#### Run indexcov and filter out scaffolds that are empty: grep -f chroms_2_call.txt regionlist.txt > regionlist.txt_2 && mv regionlist.txt_2 regionlist.txt
#### See R-script : other_scripts/coverage_calculation.r

## STEP 3: Run array script with sbatch -a 1-N%(N jobs to run at a time) array_freebayes.sh bamlist.txt regionlist.txt reference_fasta.fa
### Example: sbatch -a 1-250%50 array_freebayes.sh bamlist.txt regionlist.txt /mnt/users/tikn/seqdata1/bovine/reference/ensembl_umd3.1.1/Bos_taurus.UMD3.1.dna.toplevel.fa

##################### Script Start #########################

module load vcflib
#module load freebayes/1.1.0 ## has GL bug
module load freebayes/1.0.2 
module load samtools ##  version 1.3 as of Wed Jul 12 22:27:57 CEST 2017

TASK=$SLURM_ARRAY_TASK_ID
bamlist=$1
regions=$2
region=$( awk ' NR=='$TASK' { print $1 ; }' $regions)
name=$(echo $region | sed 's/:/_/')
ref=$3

# freebayes with moderate filtering.
echo "running region $region"
echo
echo "running command:"
echo
echo \
"freebayes --region $region \
   -f $ref \
   --bam-list $bamlist \
   --use-mapping-quality \
   --min-alternate-count 2 \
   --genotype-qualities \
   --min-alternate-fraction 0.2 \
   --min-coverage 10 \
   --no-mnps \
   --no-complex \
   --report-genotype-likelihood-max \
   -m 20 -q 20 \
   | vcffilter -f 'QUAL > 30' \
   | /mnt/users/gjuvslan/miniconda3/bin/vt normalize -r $ref - \
   | bgzip  > ${prefix}/${name}_QUAL_30.vcf.gz \
	&& tabix ${prefix}/${name}_QUAL_30.vcf.gz"
echo
date
freebayes --region $region \
   -f $ref \
   --bam-list $bamlist \
   --use-mapping-quality \
   --min-alternate-count 2 \
   --genotype-qualities \
   --min-alternate-fraction 0.2 \
   --min-coverage 10 \
   --no-mnps \
   --no-complex \
   --report-genotype-likelihood-max \
   -m 30 -q 20 \
   | vcffilter -f 'QUAL > 30' \
   | /mnt/users/gjuvslan/miniconda3/bin/vt normalize -r $ref - \
   | bgzip  > ${prefix}/${name}_QUAL_30.vcf.gz \
	&& tabix ${prefix}/${name}_QUAL_30.vcf.gz

## With stringent MQ and baseQual filters. From freebayes manual.
##   -m 30 -q 20 \
## --max-coverage cause error at the moment.
## --max-coverage 50000 \
