#!/usr/bin/env python

## Script to run freebayes analysis on population
import argparse
import sys
import os
import glob

parser = argparse.ArgumentParser( description='creates run file for freebayes SNPcalling in windows')
parser.add_argument("-b", "--bam_list", help="list of bam files to perform variant calling (textfile)", nargs=1)
parser.add_argument("-w", "--window", help="Window size to parallellize operation", nargs=1)
parser.add_argument("-s", "--species", help="Species name", nargs=1)
parser.add_argument("-r", "--reference_fasta", help="Reference fasta file (should be indexed with samtools faidx)", nargs=1)

class Freebayes:

	def set_windows(self, window, bam_list, reference_fasta, species):
		reference_index = open(reference_fasta+".fai","r")
		for ssc in reference_index:
			chr, length = ssc.split()[0], int(ssc.split()[1])
			if length <= 500000: ## Only parallelize for larger scaffolds/chr. 
				continue
			for bin in range(0,length,window):
				if bin+window > length:
					loc = chr+":"+str(bin)+"-"+str(length)
				else:
					loc = chr+":"+str(bin)+"-"+str(bin+window)
				self.freebayes(loc, bam_list,reference_fasta,species)
						
	def freebayes(self, loc, bam_list, reference_fasta, species):
		nameloc=loc.replace(":","_").replace("-","_")
        self.fb=open(species+"_"+nameloc+".sh","w")
		self.fb.write("#!/bin/bash\n")
		self.fb.write("#SBATCH --time=600:0:0\n")
		self.fb.write("#SBATCH -n 1\n")
		self.fb.write("#SBATCH -c 1\n")
		self.fb.write("#SBATCH --error=error_"+nameloc+"_freebayes.txt\n")
		self.fb.write("#SBATCH --job-name=AG_"+nameloc+"_Freebayes\n")
		self.fb.write("#SBATCH --partition=ABGC_Std\n")
		self.fb.write("#SBATCH --mem=16000\n")
		
		self.fb.write("export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lustre/backup/WUR/ABGC/shared/ABGC_Projects/STW_Deleterious_Alleles/Martijn_PHD/progs_nobackup/freebayes/vcflib/tabixpp/htslib\n")
		self.fb.write("time freebayes -f "+reference_fasta+" --bam-list "+bam_list+" -r "+loc+" --min-base-quality 10 --min-mapping-quality 20 --min-alternate-fraction 0.2 --haplotype-length 0 --ploidy 2 --min-alternate-count 2 | vcffilter -f 'QUAL > 20' | vcfkeepgeno - GT DP AD | bgzip -c > "+species+"_"+nameloc+".vcf.gz")
		
if __name__=="__main__":

	F=Freebayes()
	args = parser.parse_args()
    bam_list = args.bam_list[0]
	window = int(args.window[0])
	species = args.species[0]
	reference_fasta = args.reference_fasta[0]

	F.set_windows(window, bam_list, reference_fasta, species)
