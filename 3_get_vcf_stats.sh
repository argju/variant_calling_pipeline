#!/bin/bash
#SBATCH -J vcf_stats
#SBATCH --output=slurm_logs/merge_%j.log

module load samtools

set -o nounset   # Prevent unset variables from been used.
set -o errexit   # Exit if error occurs

vcf_in=$1
out=$(basename ${vcf_in} .vcf.gz)

mkdir -p chrN_stats

echo -e "QUAL\tDP\tAO\tCHROM\tPOS\tREF\tALT\tSAR\tSAF\tSAP" > \
	chrN_stats/${out}_info_metrics.txt && \
	bcftools query -f '%QUAL\t%DP\t%AO\t%CHROM\t%POS\t%REF\t%ALT\t%SAR\t%SAF\t%SAP\n' \
	$vcf_in >> chrN_stats/${out}_info_metrics.txt

