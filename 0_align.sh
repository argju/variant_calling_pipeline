#!/bin/sh
#SBATCH -N 1
#SBATCH -n 8
#SBATCH -J bwa
#SBATCH --mem-per-cpu=5G
#SBATCH -p cigene,centos7
##SBATCH -p hugemem
#SBATCH --output=slurm_logs/aligment_%A_%a.log

## input 1 : samples file
##
## Columns: READ1 READ2 READGROUP BAMSTEM
## READ1        path to fastq file for read 1
## READ2        path to fastq file for read 2
## READGROUP    readgroup for READ1 & READ2 ( http://gatkforums.broadinstitute.org/gatk/discussion/6472/read-groups )
## BAMSTEM      filestem for resulting bam file
## 
## input 2 : reference (bwa indexed)
##
## example: sbatch -a 1-10 pig/align.input /mnt/SeqData3/porcine/reference/GCF_000003025.6_Sscrofa11.1_genomic.fna

module load bcbio/development #newest versions of bwa, samblaster and samtools

set -o nounset   # Prevent unset variables from been used.
set -o errexit   # Exit if error occurs

CPUs=$SLURM_CPUS_ON_NODE
MEM=$SLURM_MEM_PER_CPU"M"
echo "MEM="$MEM
SAMPLES=$1
REF=$2
TASK=$SLURM_ARRAY_TASK_ID

   READ1=$(awk ' NR=='$TASK' { print $1 ; }' $SAMPLES)
   READ2=$(awk ' NR=='$TASK' { print $2 ; }' $SAMPLES)
   RG=$(awk ' NR=='$TASK' { print $3 ; }' $SAMPLES)
   BAMSTEM=$(awk ' NR=='$TASK' { print $4 ; }' $SAMPLES)

   PREFIX=bamfiles         # folder prefix
   mkdir -p $PREFIX
   BAM=${PREFIX}/${BAMSTEM}_aln.dedup.fixmate.sort.bam   # file name

   if [ -f $BAM".bai" ]
   then
     echo "Skipping task, BAM file exists "$BAM
   else
    echo "TASK starting: RG: $RG BAM filestem: $BAMSTEM"
    time bwa mem -t $CPUs -M -R $RG $REF $READ1 $READ2 | \
    samblaster | \
    samtools fixmate -O sam - - | \
    samtools view -u - | \
    samtools sort -m $MEM -@ $CPUs -O bam -T "$TMPDIR"/"$USER"/${BAMSTEM} - \
    > $BAM && \
    samtools index -@ $CPUs $BAM
   fi
