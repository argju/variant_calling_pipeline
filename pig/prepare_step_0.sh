## compile input file for step 0 (alignment)
## 
## Columns: READ1 READ2 READGROUP BAMSTEM
## READ1 	path to fastq file for read 1
## READ2 	path to fastq file for read 2
## READGROUP 	readgroup for READ1 & READ2 ( http://gatkforums.broadinstitute.org/gatk/discussion/6472/read-groups )
## BAMSTEM      filestem for resulting bam file

#list all fastq files (in1.fq and in2.fq), paired end -> two files per line
ls -d -1 /mnt/SeqData3/porcine/C101HW17120763/raw_data/*/*.gz | sed 'N;s/\n/ /' > align.tmp
ls -d -1 /mnt/SeqData3/porcine/C101HW16121086/raw_data/*/*.gz | sed 'N;s/\n/ /' >> align.tmp
sed -i s/Pig_WUR_/PigWUR/g align.tmp

#add readgroup info
gawk -F [/,_] '{print $$0" @RG\\tID:"$9"_"$10"_"$11"_"$12"\\tSM:"$9"\\tLB:"$10"\\tPU:"$11"."$12"."$9"\\tPL:ILLUMINA "}' align.tmp > align.tmp2

#add output bamfile filestem
gawk -F [/,_] '{print $0" "$9"_"$10"_"$11"_"$12}' align.tmp2 > align.input
rm align.tmp align.tmp2
sed -i s/PigWUR/Pig_WUR_/g align.input
