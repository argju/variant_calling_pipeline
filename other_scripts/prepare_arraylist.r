library(stringr)
library(dplyr)
library(readr)

all_fastqs <-
  read_csv(
    "fastq_list_2.txt",
    col_names = FALSE
  )


all_fastqs_paired <-  tibble::tibble(pair1 = all_fastqs$X1[seq(1,nrow(all_fastqs), 2)],
               pair2 = all_fastqs$X1[seq(2,nrow(all_fastqs), 2)])


all_fastqs_paired <-
  mutate(all_fastqs_paired, 
         sample = str_extract(string = pair1, "Sample_[0-9]{1,2}-cooke-[A-Z0-9]{3}"),
         id = str_split(pair1, "/", simplify = T)[,11] %>% 
           str_replace("_R1_001", ""))

write_tsv(
  all_fastqs_paired,
  "array_list_fastqs_last_batch.txt",
  col_names = F
)


