#!/bin/bash
module load snpeff
zcat merged_chrom_vcf/all_chroms_with_unplaced.filt.snpEff.vcf.gz | vcfEffOnePerLine.pl | snpSift extractFields - ID CHROM POS REF ALT ANN[*].EFFECT ANN[*].BIOTYPE ANN[*].DISTANCE ANN[*].ERRORS ANN[*].GENE ANN[*].IMPACT > snpeff_annotation.all_with_unplaced.filt.txt
