library(data.table)
library(ggplot2)
library(readr)
library(dplyr)

data <- fread("chrN_stats/all_chrom.q30_info_metrics.txt")

qplot(data = data, QUAL, DP, alpha = 0.1) + scale_x_log10() 


ggplot(sample_frac(data, .01), aes(DP/QUAL))  + geom_density()
depth_filt = median(data$DP + (2*sd(data$DP)))

plot1 <- ggplot(filter(sample_n(data, 1e6),DP < 20000),
  aes(DP)) +  
  geom_histogram(binwidth = 30) +
  coord_cartesian(xlim = c(0, 8000)) +
  geom_vline(xintercept = depth_filt) +
  theme(legend.position = "none", axis.text = element_text(size = 3)) #+
  #facet_wrap( ~ CHROM, ncol = 6)

ggsave("depth_plot.png", plot1 + theme_classic())

