#!/bin/bash
#SBATCH -N 1
#SBATCH -n 3
#SBATCH --partition=cigene,hugemem
#SBACTH -output=slurm_logs/gt_refine_%J.log

set -o nounset   # Prevent unset variables from been used.
set -o errexit   # Exit if error occurs

module load java
#module load samtools 

mkdir -p final_filt_VCFs

vcf_in=$1
vcf_out=final_filt_VCFs/$(basename ${vcf_in} .vcf.gz).refined
beagle=/mnt/users/tikn/bioinf_tools/beagle4/beagle.21Jan17.6cc.jar
nt=$SLURM_CPUS_ON_NODE
#nt=4 # For testing. 


#java -jar $beagle \
#	gl=$vcf_in \
#	out="$out" \
#	ne=200 \
#	nthreads=$nt \
#	err=0.02 \
#	overlap=20000 \
#	window=100000 \
#	niterations=10 && \
#	bcftools filter -e "AR2 < 0.7" ${out}.vcf.gz | \
#	java -jar $beagle gt=/dev/stdin  out=${vcf_out}_phased nthreads=$nt ne=200 err=0.01 \
#	overlap=20000 window=100000 niterations=10 && \
#	tabix ${vcf_out}_phased.vcf.gz && \
#	rm ${out}.*


java -jar $beagle \
	gl=$vcf_in \
	out="$vcf_out" \
	ne=200 \
	nthreads=$nt \
	err=0.02 \
	overlap=20000 \
	window=100000 \
	niterations=10 && \
tabix "$vcf_out".vcf.gz


