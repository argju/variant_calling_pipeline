library(dplyr)

indexcov_indexcov_bed <- read_delim("project/AquaGen/Rawdata/salmon/cooke_chip/alignments/indexcov/indexcov-indexcov.bed.gz",
"\t", escape_double = FALSE, trim_ws = TRUE)
x_tidyr <- tidyr::gather(indexcov_indexcov_bed, key = sample, value = cov, -c(`#chrom`, start, end))
dp <- group_by(x_tidyr, `#chrom`) %>% summarise(cov_mena = mean(cov))
hist(-log10(dp$cov_mena), breaks = 50)
x_filt <- filter(dp, cov_mena < 10, cov_mena > 0.001)
write_csv(select(x_filt, 1), path = "project/AquaGen/Rawdata/salmon/cooke_chip/chroms_2_call.txt", col_names = F)
