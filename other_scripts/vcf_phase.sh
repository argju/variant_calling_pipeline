#!/bin/bash
#SBATCH -N 1
#SBATCH -n 32
#SBATCH --partition=cigene,hugemem

set -o nounset   # Prevent unset variables from been used.
set -o errexit   # Exit if error occurs

module load java
module load samtools 

vcf_in=$1
#out=$(mktemp)
vcf_out=$(basename ${vcf_in} .vcf.gz).phased
beagle=~tikn/bioinf_tools/beagle4/beagle.21Jan17.6cc.jar
nt=$SLURM_CPUS_ON_NODE
#nt=4 # For testing. 


#java -jar $beagle \
#	gl=$vcf_in \
#	out="$out" \
#	ne=200 \
#	nthreads=$nt \
#	err=0.02 \
#	overlap=20000 \
#	window=100000 \
#	niterations=10 && \
#	bcftools filter -e "AR2 < 0.7" ${out}.vcf.gz | \
#	java -jar $beagle gt=/dev/stdin  out=${vcf_out}_phased nthreads=$nt ne=200 err=0.01 \
#	overlap=20000 window=100000 niterations=10 && \
#	tabix ${vcf_out}_phased.vcf.gz && \
#	rm ${out}.*


java -jar $beagle \
	gt=$vcf_in \
	out="$vcf_out" \
	ne=200 \
	nthreads=$nt \
	err=0.01 \
	overlap=30000 \
	window=200000 \
	niterations=10 && \
tabix "$vcf_out".vcf.gz


