"""
Pass through only lines with a column that has more than 2 commas.

python -m count_commas < test.txt
chr2       123         654,654,654,654,6676     654,654,654
"""

from __future__ import print_function

import sys

for line in sys.stdin:
	if all(word.count(",") <= 2 for word in line.split()):
		print(line, end="")
