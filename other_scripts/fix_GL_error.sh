#!/bin/bash
#SBATCH -N 1
#SBATCH -n 16

## With freebayes 1.1, he GL-fields are malformatted for som samples. Need to remove them for
## Beagle to take the GL= argument. 

vcf_in=$1
vcf_out=$(basename ${vcf_in} .vcf.gz)
chrom=19  ## WARNING: CHANGE!! ## 

module load bcftools

bcftools query  -f '[%POS\t%GL\n]' ${vcf_in} | \
	awk -F  "," 'NF>3 {print $1}' | \
	cut -f1 | uniq | \
	awk -v chr="$chrom" '{print "Chr"chr"\t"$1}' > to_remove_chr${chrom}.txt


## Remove with bcftools 
#sbatch -N 1 -n 16 --wrap "bcftools view -T ^to_remove_chr${chrom}.txt -o Chr${chrom}_full_chrom.q30.filt.norm.setID.fixGL.vcf.gz -O z \
#	--threads 16  $vcf_in" 
