# BWA and Freebayes variant calling and filter pipeline.

Documentation are included in the header of each numbered file for now.

See [this tutorial](https://github.com/ekg/alignment-and-variant-calling-tutorial) 
made by Erik Garrison for more information about the process of calling variants. 


## Future development
Update documentation.

Make core of repository species generic, which can be cloned or forked for every new alignment. Unsure how we should organize how the core code is updated. 

Use python argparse and sh to run the sbatch commands with dependencies. 
