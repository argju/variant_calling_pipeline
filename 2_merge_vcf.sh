#!/bin/sh
#SBATCH -J merge 
#SBATCH -n 16
#SBATCH -N 1
#SBATCH -p cigene,hugemem
#SBATCH --output=slurm_logs/merge_%j.log

## Concat called variants to chrom-wise files, removing duplicated variants. 

module load samtools
module load bcftools

mkdir -p merged_vcf
CHR=$1

time bcftools concat -O z --threads 16 -o merged_vcf/Chr"$CHR".q30.vcf.gz $(ls variant_calling/Chr"$CHR"_*QUAL_30.vcf.gz | sort -V)  && \
	        tabix -p vcf merged_vcf/Chr"$CHR".q30.vcf.gz
