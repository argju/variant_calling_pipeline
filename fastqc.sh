#!/bin/bash
#SBATCH -J fastqc
#SBATCH --output=slurm_logs/fastqc-%j.out

set -o nounset   # Prevent unset variables from been used.
set -o errexit   # Exit if error occurs

module load fastqc
mkdir -p fastqc_reports

for file in `cat cow/fastqfiles`
do 
  echo "fastqc_reports/"$(basename $file .fq.gz)"_started"
  if [ -e "fastqc_reports/"$(basename $file .fq.gz)"_started" ] 
  then 
    echo "fastqc_reports/"$(basename $file .fq.gz)"_started exists, skipping "$file
  else
     touch fastqc_reports/$(basename $file .fq.gz)_started
     fastqc -o fastqc_reports $file
  fi
done
