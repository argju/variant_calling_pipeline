## compile input file for step 0 (alignment)
## 
## Columns: READ1 READ2 READGROUP BAMSTEM
## READ1 	path to fastq file for read 1
## READ2 	path to fastq file for read 2
## READGROUP 	readgroup for READ1 & READ2 ( http://gatkforums.broadinstitute.org/gatk/discussion/6472/read-groups )
## BAMSTEM      filestem for resulting bam file
## SAMPLE


#list all fastq files (in1.fq and in2.fq), paired end -> two files per line
ls -d -1 /net/fs-1/results03/SeqData/norstore/bovine/Novogene_200_bulls_2019/raw*/*/*.gz | sed 'N;s/\n/ /' > align_2019_200bulls.tmp
ls -d -1 /net/fs-1/results03/SeqData/norstore/bovine/Novogene_225_bulls_2018/raw_data/*/*.gz | sed 'N;s/\n/ /' > align_2018_225bulls.tmp
ls -d -1 /net/fs-1/results03/SeqData/norstore/bovine/ullevaal_31_cows_okt_2014/*/Sample*/*.gz | sed 'N;s/\n/ /' > align_2014_31cows.tmp
ls -d -1 /net/fs-1/results03/SeqData/norstore/bovine/ullevaal_96_bulls_aug_2015/150821_7001448.B.Project_Kent-cowDNA-2015-06-19/Sample_*/*.gz | sed 'N;s/\n/ /' > align_2015_96bulls.tmp

ls -d -1 /mnt/users/gjuvslan/geno/variant_calling_pipeline/cow/1000bulls96/*/*fastq.gz | sed 'N;s/\n/ /' > align_2015_96bulls.tmp

#add readgroup info
gawk -F [/,_] '{print $$0" @RG\\tID:"$15"_"$16"_"$17"_"$18"\\tSM:"$15"\\tLB:"$16"\\tPU:"$17"."$18"."$15"\\tPL:ILLUMINA"}' align_2019_200bulls.tmp > align_2019_200bulls.tmp2
gawk -F [/,_] '{print $$0" @RG\\tID:"$15"_"$16"_"$17"_"$18"\\tSM:"$15"\\tLB:"$16"\\tPU:"$17"."$18"."$15"\\tPL:ILLUMINA"}' align_2018_225bulls.tmp > align_2018_225bulls.tmp2
gawk '{print $1}' align_2014_31cows.tmp | xargs basename -a -s .fastq.gz | sed s/Storfe_rerun/Storfe-rerun/ > 2014_31cows.tmp
gawk -F"_" '{print "@RG\\tID:"$1"_LIBRARY_FLOWCELL_"$3"\\tSM:"$1"\\tLB:LIBRARY\\tPU:FLOWCELL."$3"."$1"\\tPL:ILLUMINA"}' 2014_31cows.tmp | sed s/Storfe-rerun/Storfe_rerun/ > 2014_31cows.tmp2
paste align_2014_31cows.tmp 2014_31cows.tmp2 > align_2014_31cows.tmp2
grep "\/aug\/" align_2015_96bulls.tmp | gawk '{print $1}' | xargs basename -a -s .fastq.gz  > 2015_96bulls_aug.tmp
grep -v "\/aug\/" align_2015_96bulls.tmp | gawk '{print $1}' | xargs basename -a -s .fastq.gz  > 2015_96bulls_sepoct.tmp
gawk -F"_" '{print "@RG\\tID:"$1"_LIBRARY_FLOWCELL_"$3"\\tSM:"$1"\\tLB:LIBRARY\\tPU:FLOWCELL."$3"."$1"\\tPL:ILLUMINA"}' 2015_96bulls_aug.tmp > 2015_96bulls.tmp2
gawk -F"_" '{print "@RG\\tID:"$1"_LIBRARY_FLOWCELL_"$3"x"$4"x"$5"\\tSM:"$1"\\tLB:LIBRARY\\tPU:FLOWCELL."$3"x"$4"x"$5"."$1"\\tPL:ILLUMINA"}' 2015_96bulls_sepoct.tmp >> 2015_96bulls.tmp2
paste align_2015_96bulls.tmp 2015_96bulls.tmp2 > align_2015_96bulls.tmp2

## add bamstem and sample columns
gawk -F : '{print $2}' align_2019_200bulls.tmp2 | gawk -F\\ '{print $1}' > bamstem
gawk -F : '{print $3}' align_2019_200bulls.tmp2 | gawk -F\\ '{print $1}' > sample 
paste align_2019_200bulls.tmp2 bamstem sample | tr '\t' ' '  > align_2019_200bulls.input && rm *2019_200bulls.tmp* 
gawk -F : '{print $2}' align_2018_225bulls.tmp2 | gawk -F\\ '{print $1}' > bamstem
gawk -F : '{print $3}' align_2018_225bulls.tmp2 | gawk -F\\ '{print $1}' > sample 
paste align_2018_225bulls.tmp2 bamstem sample | tr '\t' ' '  > align_2018_225bulls.input && rm *2018_225bulls.tmp* 
gawk -F : '{print $2}' align_2014_31cows.tmp2 | gawk -F\\ '{print $1}' > bamstem
gawk -F : '{print $3}' align_2014_31cows.tmp2 | gawk -F\\ '{print $1}' > sample 
paste align_2014_31cows.tmp2 bamstem sample | tr '\t' ' ' > align_2014_31cows.input && rm *2014_31cows.tmp*
gawk -F : '{print $2}' align_2015_96bulls.tmp2 | gawk -F\\ '{print $1}' > bamstem
gawk -F : '{print $3}' align_2015_96bulls.tmp2 | gawk -F\\ '{print $1}' > sample
paste align_2015_96bulls.tmp2 bamstem sample | tr '\t' ' ' > align_2015_96bulls.input && rm *2015_96bulls*.tmp*
rm bamstem sample

## count lines and sample ids per input file
for file in *.input
do
    echo "** Input file "$file
    echo "* N lines:    "$(wc -l $file| cut -d" " -f1)
    ids=$(gawk '{print $4}' $file | sort | uniq | wc -l)
    echo "* N bamfiles: "$ids
    samples=$(gawk '{print $5}' $file | sort | uniq | wc -l)
    echo "* N samples:  "$samples
done
